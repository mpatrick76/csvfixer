package com.cds.csv.fix.java;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FixCsvFile {
	public static void main(String[] args) throws FileNotFoundException {

		String workingDir = "d://ftp//et//fix//";
		String file = args.length > 0 ? args[0] : "";
		String directory = args.length > 1 ? args[1] : "";
		
		if ("".equals(file)) {
			file = "Bounces.txt";
		}
		
		if ("".equals(directory)) {
			directory = "20160614054004";
		}
		
		String filename = workingDir + directory + "//" + file;
		
		new File(workingDir + directory + "//fixed").mkdir();
		
		Writer writer = null; 
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(filename));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(workingDir + directory + "//fixed//" + file)));
			String line = null;
			String savedLine = "";
			
			while ((line = reader.readLine()) != null ) {
				if (line.endsWith(",")) {
					line = line.concat(" ");  // Make sure the last token gets counted
				}
				
				StringTokenizer savedTokenizer = new StringTokenizer(savedLine, ",", false);
				List<String> savedLineTokens = new ArrayList<>();

				while (savedTokenizer.hasMoreTokens()) {
					String token = savedTokenizer.nextToken();
					savedLineTokens.add(token.trim());
				}
				
				if (savedLineTokens.size() >= 12) {
					writer.write(savedLine + "\r\n");
					savedLine = line;
					continue;
				}
				
				if (line.length() == 0) {
					continue;
				}
				
				StringTokenizer tokenizer = new StringTokenizer(line, ",", false);
				List<String> tokens = new ArrayList<>();

				while (tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					tokens.add(token.trim());
				}
				
				if (tokens.size() > 2) {
					savedLine = line;
					continue;
				}
				
				if (tokens.size() == 1 || tokens.size() == 2) {
					savedLine = savedLine.concat(line);
					continue;
				}
			}
			
			writer.close();
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
