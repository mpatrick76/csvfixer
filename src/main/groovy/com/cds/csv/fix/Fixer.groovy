package com.cds.csv.fix

import groovy.transform.CompileStatic

@CompileStatic
class Fixer {

	public static void main(String[] args) throws FileNotFoundException {
		
		String workingDir = "d://ftp//et//fix//"
		String file = args.length > 0 ? args[0] : ""
		String directory = args.length > 1 ? args[1] : ""
		
		if ("".equals(file)) {
			file = "Bounces.csv";
		}
		
		if ("".equals(directory)) {
			directory = "20160614054004";
		}
		
		new File("${workingDir}${directory}//fixed").mkdir()
		
		try {
			String line = null;
			String savedLine = ""
			
			File inFile = new File("${workingDir}${directory}//${file}")
			File outFile = new File("${workingDir}${directory}//fixed//${file}")
			
			inFile.eachLine { String fileLine ->
				if (fileLine.endsWith(",")) {
					fileLine = fileLine.concat(" ")
				}
				
				List<String> savedFileLineTokens = savedLine.tokenize(',');
				
				if (savedFileLineTokens.size() >= 12) {
					outFile << "${savedLine}\r\n"
					savedLine = fileLine
					return
				}
				
				if (fileLine.length() == 0) {
					return
				}
				
				List<String> fileTokens = fileLine.tokenize(',');
				
				if (fileTokens.size() > 2) {
					savedLine = fileLine
					return
				}
				
				if (fileTokens.size() == 1 || fileTokens.size() == 2) {
					savedLine = savedLine.concat(fileLine)
					return
				}
			}
			println "Done"
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
